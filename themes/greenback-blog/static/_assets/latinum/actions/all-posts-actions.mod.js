import Logger from "/shared/logger/shared-logger.mod.js";
import { AllPostsPopulator } from "../../oscar/blog/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("all-posts");

var allPostsAxns = new function() {

	var allPop = new AllPostsPopulator("#allPosts");
	allPop.setItemsPerPage(10);

	this.loadPage = function() {
		allPop.setup();
	}

	this.unloadPage = function() {
		allPop.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("allPostsAxns", allPostsAxns);
