import Logger from "/shared/logger/shared-logger.mod.js";
import { SinglePostPopulator } from "../../oscar/blog/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("greenback.blog.onepost");

var singlePostAxns = new function() {

	var pop = null;

	this.loadLayout = function(page, params, query) {
		_L.info("layout loaded");
		pop = new SinglePostPopulator(latinum.siteId(), "pageTop", "pageBot");
		pop.setupLayout();
	}

	this.loadPage = function(page, params, query) {
		_L.info("page loaded");
		if(pop != null) {
			pop.setup(page.slug);
		}
	}

	this.unloadPage = function() {
		_L.info("page unloaded");
		if(pop != null) {
			pop.teardown();
		}
	}

	this.unloadLayout = function() {
		_L.info("layout unloaded");
		if(pop != null) {
			pop.teardownLayout();
		}
		pop = null;
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("singlePostAxns", singlePostAxns);
