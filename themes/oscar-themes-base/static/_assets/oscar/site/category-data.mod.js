import Logger from "/shared/logger/shared-logger.mod.js";
import XmlUtils from "../xmlutils.mod.js";

var _L = Logger.get("oscar.site.data");

var categoryData = new function() {

	var page2CatMap = {};
	var allCategories = {};
	var loadProm = null;

	this.load = function() {
		loadProm = fetchRemoteData("blog_categories/index.xml", "xml")
			.then(parseCategoriesData)
			.then(loadEachCategory)
			.then(resolvePosts2Category);
		return loadProm;
	}

	this.getAllCategories = function() {
		let impl = function() {
			let catlist = new Array();
			for(catId in allCategories) {
				catlist.push(allCategories[catId]);
			}
			_L.info("category list: " + catlist);
			return Promise.resolve(catlist);
		}
		return loadProm.then(impl);
	}

	this.getCategory = function(catId) {
		let impl = function() {
			if(allCategories[catId]) {
				return Promise.resolve(allCategories[catId]);
			}
			return Promise.resolve(null);
		}
		return loadProm.then(impl);
	}

	this.getCategoriesForPage = function(slug) {
		let impl = function() {
			if(page2CatMap[slug]) {
				return Promise.resolve(page2CatMap[slug]);
			}
			return Promise.resolve(new Array());
		}
		return loadProm.then(impl);
	}

	//// HELPER METHODS ////////////////////////////////////////////////////////////////////////////

	function fetchRemoteData(path, format) {
		let prom = new Promise((resolve, reject) => {
			_L.debug("fetching definitions as " + format);
			$.ajax({
				cache: false, url: path, dataType: format,
				success: function(data) {
					_L.debug("definitions fetched from " + path);
					resolve(data);
				},
				error : function(xhdr, textStatus, error) {
					_L.error("error fetching definitions from " + path);
					_L.error("    status : " + textStatus);
					_L.error("    description : " + error);
					reject(path + " : " + textStatus + " - " + error);
				}
			});
		});
		return prom;
	}

	function parseCategoriesData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		let catsCol = XmlUtils.findRootCollection(rElm, "category");
		Array.from(catsCol).forEach(_cat => {
			let category = {};
			category.slug = XmlUtils.toSimpleValue(_cat, "slug");
			category.title = XmlUtils.toSimpleValue(_cat, "title");
			category.count = XmlUtils.toSimpleValue(_cat, "pages");
			allCategories[category.slug] = category;
		});
	}

	function loadEachCategory() {
		let promarr = new Array();
		for(const slug in allCategories) {
			let prom = fetchRemoteData("categories/" + slug + "/index.xml", "xml").then(data => {
				parseEachCategory(slug, data);
			});
			promarr.push(prom);
		}
		return Promise.all(promarr);
	}

	function parseEachCategory(slug, xmlData) {
		_L.debug("parsing " + slug + " with " + xmlData);
		var rElm = XmlUtils.getRoot(xmlData);
		var category = allCategories[slug];
		if(typeof category === "undefined") {
			_L.error("that wasn't in the job description");
			return;
		}
		category.title = XmlUtils.toSimpleValue(rElm, "title");
		category.summary = XmlUtils.toSimpleValue(rElm, "summary");
		category.pages = new Array();
		let pagesCol = XmlUtils.findCollection(rElm, "pages", "page");
		Array.from(pagesCol).forEach(_page => {
			let page = {};
			page.slug = XmlUtils.toSimpleValue(_page, "slug");
			page.title = XmlUtils.toSimpleValue(_page, "title");
			page.summary = XmlUtils.toSimpleValue(_page, "summary");
			page.date = XmlUtils.toSimpleValue(_page, "date");
			category.pages.push(page);
		});
	}

	function resolvePosts2Category() {
		_L.info("categories loaded", allCategories);

		for(catId in allCategories) {
			allCategories[catId].pages.forEach(_page => {
				if(page2CatMap[_page.slug]) {
					let catList = page2CatMap[_page.slug];
					catList.push(allCategories[catId]);
				}
				else {
					let catList = new Array();
					catList.push(allCategories[catId]);
					page2CatMap[_page.slug] = catList;
				}
			});
		}

		_L.info("pages 2 categories", page2CatMap);
	}
}

/** @global */
window.$categoryData = categoryData;

export default categoryData;