import Logger from "/shared/logger/shared-logger.mod.js";

var XmlUtils = new function() {

	this.getRoot = function(xmlData) {
		return xmlData.documentElement;
	}

	this.hasValue = function(pnode, name) {
		if(pnode.hasAttribute(name)) {
			return true;
		}

		let tnode = null;
		for(var i=0; i<pnode.children.length; i++) {
			tnode = pnode.children.item(i);
			if(tnode.tagName === name) {
				return true;
			}
		}
		return false;
	}

	this.toSimpleValue = function(pnode, name) {
		if(pnode.hasAttribute(name)) {
			return pnode.getAttribute(name);
		}

		let tnode = null;
		for(var i=0; i<pnode.children.length; i++) {
			tnode = pnode.children.item(i);
			if(tnode.tagName === name) {
				break;
			}
			else {
				tnode = null;
			}
		}

		if(tnode != null && typeof tnode.textContent === "string") {
			return tnode.textContent.trim();
		}

		return null;
	}

	this.findCollection = function(parentElem, collectionName, itemName) {
		//this gets you a reference to the <_collection_> node
		let colElem = null;
		for(var i=0; i<parentElem.children.length; i++) {
			colElem = parentElem.children.item(i);
			if(colElem.tagName === collectionName) {
				break;
			}
			else {
				colElem = null;
			}
		}

		//this gets you an array of <_collection_item_> inside <_collection_>
		let retCol = new Array();
		if(colElem != null) {
			Array.from(colElem.children).forEach(node => {
				if(node.tagName === itemName) {
					retCol.push(node);
				}
			});
		}

		return retCol;
	}

	this.findRootCollection = function(parentElem, itemName) {
		//this gets you an array of <_collection_item_> inside parentElem
		let retCol = new Array();
		Array.from(parentElem.children).forEach(node => {
			if(node.tagName === itemName) {
				retCol.push(node);
			}
		});
		return retCol;
	}
}

export default XmlUtils;