import Logger from "/shared/logger/shared-logger.mod.js";
import XmlUtils from "../xmlutils.mod.js";

var _L = Logger.get("oscar.book.data");

var bookData = new function() {

	var allPages = new Array();
	var loadProm = null;

	this.load = function() {
		let prom1 = fetchRemoteData("book/index.xml", "xml").then(parseBookData);
		loadProm = prom1.then(function() {
			_L.info("pages: ", allPages);
		});
		return loadProm;
	}

	this.getFlatTree = function() {
		let impl = function() {
			var ftree = new Array();
			allPages.forEach(_page => {
				flattenTreePage(ftree, _page, 0);
			});
			_L.debug("flat tree", ftree);
			return Promise.resolve(ftree);
		};
		return loadProm.then(impl);
	}

	this.getPage = function(slug) {
		let impl = function() {
			let pageMeta = null;
			for(var i=0; i<allPages.length; i++) {
				let prev = (i > 0) ? allPages[i - 1] : null;
				let next = (i < allPages.length - 1) ? allPages[i + 1] : null;
				pageMeta = getPageNested(allPages[i], slug, prev, next);
				if(pageMeta != null) {
					break;
				}
			}
			return Promise.resolve(pageMeta);
		}
		return loadProm.then(impl);
	}

	//// HELPER METHODS ////////////////////////////////////////////////////////////////////////////

	function fetchRemoteData(path, format) {
		let prom = new Promise((resolve, reject) => {
			_L.debug("fetching definitions as " + format);
			$.ajax({
				cache: false, url: path, dataType: format,
				success: function(data) {
					_L.debug("definitions fetched from " + path);
					resolve(data);
				},
				error : function(xhdr, textStatus, error) {
					_L.error("error fetching definitions from " + path);
					_L.error("    status : " + textStatus);
					_L.error("    description : " + error);
					reject(path + " : " + textStatus + " - " + error);
				}
			});
		});
		return prom;
	}

	function parseBookData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		let pagesCol = XmlUtils.findRootCollection(rElm, "page");
		Array.from(pagesCol).forEach(_page => {
			allPages.push(parsePage(_page));
		});
	}

	function parsePage(_page) {
		let page = {};
		page.slug = XmlUtils.toSimpleValue(_page, "slug");
		page.title = XmlUtils.toSimpleValue(_page, "title");
		page.summary = XmlUtils.toSimpleValue(_page, "summary");
		page.nestedPages = parseNestedPages(_page);
		return page;
	}

	function parseNestedPages(_page) {
		let nestedPages = new Array();
		let npagesCol = XmlUtils.findCollection(_page, "nested-pages", "page");
		Array.from(npagesCol).forEach(_npage => {
			nestedPages.push(parsePage(_npage));
		});
		return nestedPages;
	}

	function flattenTreePage (ftree, _page, indent) {
		let tocItem = {};
		let itemStyle = "";
		if(indent == 0) {
			itemStyle += "noindent";
		}
		else {
			itemStyle += "indent" + indent;
		}
		if(_page.nestedPages.length > 0) {
			itemStyle += " node";
		}
		else {
			itemStyle += " leaf";
		}
		tocItem.style = itemStyle;
		tocItem.slug = _page.slug;
		tocItem.title = _page.title;
		tocItem.summary = _page.summary;
		ftree.push(tocItem);
		_page.nestedPages.forEach(_npage => {
			flattenTreePage(ftree, _npage, indent + 1);
		})
	}

	function getPageNested(page, slug, prevPage, nextPage) {
		if(page.slug === slug) {
			let pageMeta = {};
			pageMeta.slug = page.slug;
			pageMeta.title = page.title;
			pageMeta.summary = page.summary;
			pageMeta.previous = null;
			if(prevPage) {
				pageMeta.previous = {
					"slug" : prevPage.slug,
					"title" : prevPage.title,
					"summary" : prevPage.summary
				};
			}
			pageMeta.next = null;
			if(nextPage) {
				pageMeta.next = {
					"slug" : nextPage.slug,
					"title" : nextPage.title,
					"summary" : nextPage.summary
				};
			}
			return pageMeta;
		}
		//else
		for(var i=0; i<page.nestedPages.length; i++) {
			let prev = (i > 0) ? page.nestedPages[i - 1] : null;
			let next = (i < page.nestedPages.length - 1) ? page.nestedPages[i + 1] : null;

			let pageMeta = getPageNested(page.nestedPages[i], slug, prev, next);
			if(pageMeta != null) {
				return pageMeta;
			}
		}
		return null;
	}
}

/** @global */
window.$bookData = bookData;

export default bookData;