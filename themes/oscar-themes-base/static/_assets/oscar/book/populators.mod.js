import Logger        from "/shared/logger/shared-logger.mod.js";
import { createApp } from "/shared/vue/vue.esm-browser.prod.js";
import bookData      from "./book-data.mod.js";

var _L = Logger.get("oscar.booklet.populator");

var TocPopulator = function(nodeId) {

	_L.debug("nodeId : " + nodeId);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var vapp = null;

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"tocItems" : new Array()
				};
			}
		});

		let vm = vapp.mount(nodeId);

		bookData.getFlatTree().then(tocItems => {
			for(var i=0; i<tocItems.length; i++) {
				vm.$data.tocItems.push(tocItems[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}

var BookPagePopulator = function(siteId, topNodeId, botNodeId) {

	_L.debug("siteId : " + siteId, "topNodeId : " + topNodeId, "botNodeId : " + botNodeId);

	topNodeId = (topNodeId.charAt(0) != '#') ? ("#" + topNodeId) : topNodeId;
	botNodeId = (botNodeId.charAt(0) != '#') ? ("#" + botNodeId) : botNodeId;

	var tvapp = null, tvm = null, bvapp = null, bvm = null;

	/**
	 * Method to be called whenever the corresponding latinum layout gets loaded.
	 */

	this.setupLayout = function() {
		tvapp = createApp({
			data() {
				return {
					"summary" : null,
					"previous" : null, "next" : null
				};
			}
		});
		tvm = tvapp.mount(topNodeId);

		bvapp = createApp({
			data() {
				return {
					"previous" : null, "next" : null,
					"commentsEnabled" : false,
				};
			}
		});
		bvm = bvapp.mount(botNodeId);
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function(slug) {

		_L.debug("slug : " + slug);

		bookData.getPage(slug).then(page => {

			if(page == null) {
				_L.warn("page not found : " + slug);
				return;
			}
			//else ...

			if(tvm != null) {
				tvm.$data.summary = page.summary;
				tvm.$data.previous = page.previous;
				tvm.$data.next = page.next;
			}

			if(bvm != null) {
				bvm.$data.commentsEnabled = page.commentsEnabled;
				bvm.$data.previous = page.previous;
				bvm.$data.next = page.next;
			}

			//setup disqus
			let d = document;
			if(d.getElementById("disqus_thread")) {
				$("#disqus_thread").html("");
				if(page.commentsEnabled && siteId) {
					if(d.getElementById("disqus_script")) {
						//script is setup. just reload.
						DISQUS.reset({
							reload : true,
							config : function() {
								this.page.identifier = slug;
								this.page.url = window.location.href;
							}
						});
					}
					else {
						//setup the disqus script.
						window.disqus_config = function() {
							this.page.identifier = slug;
							this.page.url = window.location.href;
						};
						let s = d.createElement("script");
						s.id = "disqus_script"; //so that we can look it up, as in above
						s.type = 'text/javascript'; s.async = true;
						s.src = "https://" + siteId + ".disqus.com/embed.js";
						(d.head || d.body).appendChild(s);
					}
				}
			}
			// end setup disqus
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(tvm != null) {
			tvm.$data.summary = null;
			tvm.$data.date = null;
			tvm.$data.categories = new Array();
			tvm.$data.previous = null;
			tvm.$data.next = null;
		}

		if(bvm != null) {
			bvm.$data.commentsEnabled = false;
			bvm.$data.previous = null;
			bvm.$data.next = null;
		}

		/* todo: enable this later
		if(document.getElementById("disqus_thread")) {
			$("#disqus_thread").html("");
		}
		*/
	}

	/**
	 * Method to be called whenever the corresponding latinum layout gets unloaded.
	 */

	this.teardownLayout = function() {
		if(tvapp != null) {
			tvapp.unmount(topNodeId);
			tvapp = null;
		}
		tvm = null;

		if(bvapp != null) {
			bvapp.unmount(botNodeId);
			bvapp = null;
		}
		bvm = null;
	}
}

export {
	TocPopulator, BookPagePopulator
}