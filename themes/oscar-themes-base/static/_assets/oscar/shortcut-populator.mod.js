import Logger from "/shared/logger/shared-logger.mod.js";
import { createApp } from "/shared/vue/vue.esm-browser.prod.js";
import XmlUtils from "./xmlutils.mod.js";

var _L = Logger.get("oscar.shortcut");

var ShortcutPopulator = function(nodeId, navId) {

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var count   = 0;
	var vapp    = null;
	var popData = null;

	this.setCount = function(val) {
		if(typeof val === "number") {
			count = val;
		}
	}

/**
 * Method to be called whenever the corresponding latinum page gets loaded.
 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"title" : null,
					"summary" : null,
					"shortcuts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		if(popData == null) {
			let dataPath = "shortcut_lists/" + navId + "/index.xml";
			_L.debug("definitions path: " + dataPath);
			fetchRemoteData(dataPath, "xml")
			.then(data => {
				parseData(data);
				updateTemplate(vm);
			}).catch(err => {
				_L.error(err);
			});
		}
		else {
			_L.debug("shortcuts data available for " + navId + ". populating");
			updateTemplate(vm);
		}
	}

/**
 * Method to be called whenever the corresponding latinum page gets unloaded.
 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}

	//// HELPER METHODS ////////////////////////////////////////////////////////////////////////////

	function fetchRemoteData(path, format) {
		let prom = new Promise((resolve, reject) => {
			_L.debug("fetching definitions as " + format);
			$.ajax({
				cache: false, url: path, dataType: format,
				success: function(data) {
					_L.debug("definitions fetched from " + path);
					resolve(data);
				},
				error : function(xhdr, textStatus, error) {
					_L.error("error fetching definitions from " + path);
					_L.error("    status : " + textStatus);
					_L.error("    description : " + error);
					reject(path + " : " + textStatus + " - " + error);
				}
			});
		});
		return prom;
	}

	function parseData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		popData = {};
		popData.title = XmlUtils.toSimpleValue(rElm, "title");
		popData.summary = XmlUtils.toSimpleValue(rElm, "summary");
		popData.shortcuts = new Array();
		let scutCol = XmlUtils.findCollection(rElm, "shortcuts", "shortcut");
		let limit = 0;
		Array.from(scutCol).forEach(scut => {
			if(count > 0 && limit >= count) {
				return;
			}
			let shortcut = {};
			shortcut.title = XmlUtils.toSimpleValue(scut, "title");
			shortcut.summary = XmlUtils.toSimpleValue(scut, "summary");
			shortcut.longDesc = XmlUtils.toSimpleValue(scut, "long-desc");
			shortcut.location = XmlUtils.toSimpleValue(scut, "location");
			shortcut.thumbnail = XmlUtils.toSimpleValue(scut, "thumbnail");
			popData.shortcuts.push(shortcut);
			limit++;
		});
	}

	function updateTemplate(vm) {
		vm.$data.title = popData.title;
		vm.$data.summary = popData.summary;
		popData.shortcuts.forEach(scut =>{
			vm.$data.shortcuts.push(scut);
		});
	}
}

export default ShortcutPopulator;