import Logger from "/shared/logger/shared-logger.mod.js";
import { createApp } from "/shared/vue/vue.esm-browser.prod.js";
import blogData from "./blog-data.mod.js";

var _L = Logger.get("oscar.blog.populator");

/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * ALL CATEGORIES POPULATOR
 *
 * @param {*} nodeId
 */

var AllCategoriesPopulator = function(nodeId) {

	_L.debug("nodeId : " + nodeId);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var vapp  = null;

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"categories" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		blogData.getAllCategories().then(categories => {
			for(var i=0; i<categories.length; i++) {
				vm.$data.categories.push(categories[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}



/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * SINGLE CATEGORY POPULATOR
 *
 * @param {*} nodeId
 * @param {*} slug
 */

var SingleCategoryPopulator = function(nodeId, slug) {

	_L.debug("nodeId : " + nodeId + ", slug : " + slug);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var ipp = 0;
	var vapp  = null;

	this.setItemsPerPage = function(val) {
		if(typeof val === "number") {
			ipp = val;
		}
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"category" : {
						"title" : "",
						"summary" : "",
						"count" : 0
					},
					"posts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		blogData.getCategory(slug).then(category => {
			if(category == null) {
				_L.warn("category not found with slug : " + slug);
				return;
			}
			vm.$data.category.title = category.title;
			vm.$data.category.summary = category.summary;

			for(var i=0; i<category.posts.length; i++) {
				vm.$data.posts.push(category.posts[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}



/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * PINNED POSTS POPULATOR
 *
 * @param {*} nodeId
 */

var PinnedPostsPopulator = function(nodeId) {

	_L.debug("nodeId : " + nodeId);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var count = 0;
	var vapp  = null;

	this.setCount = function(val) {
		if(typeof val === "number") {
			count = val;
		}
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"posts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		blogData.getPinnedPosts().then(posts => {
			for(var i=0; i<posts.length; i++) {
				if(i >= count) {
					break;
				}
				vm.$data.posts.push(posts[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}



/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * LATEST POSTS POPULATOR
 *
 * @param {*} nodeId
 */

var LatestPostsPopulator = function(nodeId) {

	_L.debug("nodeId : " + nodeId);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var count = 0;
	var vapp  = null;

	this.setCount = function(val) {
		if(typeof val === "number") {
			count = val;
		}
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"posts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		blogData.getAllPosts().then(posts => {
			for(var i=0; i<posts.length; i++) {
				if(i >= count) {
					break;
				}
				vm.$data.posts.push(posts[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}



/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * ALL POSTS POPULATOR
 *
 * @param {*} nodeId
 */

var AllPostsPopulator = function(nodeId) {

	_L.debug("nodeId : " + nodeId);

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var ipp  = 0;
	var vapp = null;

	this.setItemsPerPage = function(val) {
		if(typeof val === "number") {
			ipp = val;
		}
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function() {
		vapp = createApp({
			data() {
				return {
					"posts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		blogData.getAllPosts().then(posts => {
			for(var i=0; i<posts.length; i++) {
				vm.$data.posts.push(posts[i]);
			}
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}
}



/**
 * .::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.::.
 * SINGLE POST POPULATOR
 *
 * @param {*} nodeId
 * @param {*} slug
 */

var SinglePostPopulator = function(siteId, topNodeId, botNodeId) {

	_L.debug("siteId : " + siteId, "topNodeId : " + topNodeId, "botNodeId : " + botNodeId);

	topNodeId = (topNodeId.charAt(0) != '#') ? ("#" + topNodeId) : topNodeId;
	botNodeId = (botNodeId.charAt(0) != '#') ? ("#" + botNodeId) : botNodeId;

	var tvapp = null, tvm = null, bvapp = null, bvm = null;

	/**
	 * Method to be called whenever the corresponding latinum layout gets loaded.
	 */

	this.setupLayout = function() {
		tvapp = createApp({
			data() {
				return {
					"summary" : null, "date" : null, "categories" : new Array(),
					"previous" : null, "next" : null
				};
			}
		});
		tvm = tvapp.mount(topNodeId);

		bvapp = createApp({
			data() {
				return {
					"previous" : null, "next" : null,
					"commentsEnabled" : false,
				};
			}
		});
		bvm = bvapp.mount(botNodeId);
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets loaded.
	 */

	this.setup = function(slug) {

		_L.debug("slug : " + slug);

		blogData.getPost(slug).then(post => {
			if(post == null) {
				_L.warn("post not found : " + slug);
				return;
			}
			//else ...

			if(tvm != null) {
				tvm.$data.summary = post.summary;
				tvm.$data.date = post.date;
				post.categories.forEach(_cat => {
					tvm.$data.categories.push(_cat);
				});
				tvm.$data.previous = post.previous;
				tvm.$data.next = post.next;
			}

			if(bvm != null) {
				bvm.$data.commentsEnabled = post.commentsEnabled;
				bvm.$data.previous = post.previous;
				bvm.$data.next = post.next;
			}

			// setup disqus
			let d = document;
			if(d.getElementById("disqus_thread")) {
				$("#disqus_thread").html("");
				if(post.commentsEnabled && siteId) {
					if(d.getElementById("disqus_script")) {
						//script is setup. just reload.
						DISQUS.reset({
							reload : true,
							config : function() {
								this.page.identifier = slug;
								this.page.url = window.location.href;
							}
						});
					}
					else {
						//setup the disqus script.
						window.disqus_config = function() {
							this.page.identifier = slug;
							this.page.url = window.location.href;
						};
						let s = d.createElement("script");
						s.id = "disqus_script"; //so that we can look it up, as in above
						s.type = 'text/javascript'; s.async = true;
						s.src = "https://" + siteId + ".disqus.com/embed.js";
						(d.head || d.body).appendChild(s);
					}
				}
			}
			// end setup disqus
		});
	}

	/**
	 * Method to be called whenever the corresponding latinum page gets unloaded.
	 */

	this.teardown = function() {
		if(tvm != null) {
			tvm.$data.summary = null;
			tvm.$data.date = null;
			tvm.$data.categories = new Array();
			tvm.$data.previous = null;
			tvm.$data.next = null;
		}

		if(bvm != null) {
			bvm.$data.commentsEnabled = false;
			bvm.$data.previous = null;
			bvm.$data.next = null;
		}

		if(document.getElementById("disqus_thread")) {
			$("#disqus_thread").html("");
		}
	}

	/**
	 * Method to be called whenever the corresponding latinum layout gets unloaded.
	 */

	this.teardownLayout = function() {
		if(tvapp != null) {
			tvapp.unmount(topNodeId);
			tvapp = null;
		}
		tvm = null;

		if(bvapp != null) {
			bvapp.unmount(botNodeId);
			bvapp = null;
		}
		bvm = null;
	}
}



export {
	AllCategoriesPopulator,
	SingleCategoryPopulator,
	PinnedPostsPopulator,
	LatestPostsPopulator,
	AllPostsPopulator,
	SinglePostPopulator
};
