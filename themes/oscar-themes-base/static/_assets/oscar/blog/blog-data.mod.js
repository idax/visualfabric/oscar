import Logger from "/shared/logger/shared-logger.mod.js";
import XmlUtils from "../xmlutils.mod.js";

var _L = Logger.get("oscar.blog.data");

var blogData = new function() {

	var allPosts = new Array();
	var allCategories = {};
	var loadProm = null;

	this.load = function() {
		let prom1 = fetchRemoteData("blog/index.xml", "xml").then(parsePostsData);
		let prom2 = fetchRemoteData("blog_categories/index.xml", "xml").then(parseCategoriesData);
		loadProm = Promise.all([prom1, prom2]).then(loadEachCategory).then(function() {
			_L.info("posts: ", allPosts);
			_L.info("categories: ", allCategories);
		});
		return loadProm;
	}

	this.getPost = function(slug) {
		let impl = function() {
			let tpost = null, ppost = null, npost = null;
			for(var i=0; i<allPosts.length; i++) {
				if(allPosts[i].slug === slug) {
					tpost = allPosts[i];
					ppost = (i > 0) ? allPosts[i - 1] : null;
					npost = (i < allPosts.length - 1) ? allPosts[i + 1] : null;
					break;
				}
			}
			if(tpost == null) {
				return Promise.resolve(null);
			}
			var postMeta = {};
			postMeta.summary = tpost.summary;
			postMeta.date = tpost.date;
			postMeta.commentsEnabled = tpost.commentsEnabled;
			postMeta.categories = new Array();
			for(const [catSlug, category] of Object.entries(allCategories)) {
				for(const post in category.posts) {
					if(post.slug === slug) {
						let shadCat = {};
						shadCat.slug = category.slug;
						shadCat.title = category.title;
						postMeta.categories.push(shadCat);
					}
				}
			}
			if(ppost) {
				postMeta.previous = {
					"slug" : ppost.slug,
					"title" : ppost.title
				};
			}
			else {
				postMeta.previous = null;
			}
			if(npost) {
				postMeta.next = {
					"slug" : npost.slug,
					"title" : npost.title
				};
			}
			else {
				postMeta.next = null;
			}
			return Promise.resolve(postMeta);
		}
		return loadProm.then(impl);
	}

	this.getPinnedPosts = function() {
		let impl = function() {
			var pinnedPosts = new Array();
			allPosts.forEach(_post => {
				if(!_post.pinned) {
					return;
				}
				var postMeta = {};
				postMeta.slug = _post.slug;
				postMeta.title = _post.title;
				postMeta.summary = _post.summary;
				postMeta.date = _post.date;
				postMeta.thumbnail = _post.thumbnail;
				postMeta.categories = new Array();
				for(const [catSlug, category] of Object.entries(allCategories)) {
					for(const post in category.posts) {
						if(post.slug === _post.slug) {
							let shadCat = {};
							shadCat.slug = category.slug;
							shadCat.title = category.title;
							postMeta.categories.push(shadCat);
						}
					}
				}
				pinnedPosts.push(postMeta);
			});
			return Promise.resolve(pinnedPosts);
		}
		return loadProm.then(impl);
	}

	this.getAllPosts = function() {
		let impl = function() {
			var postList = new Array();
			allPosts.forEach(_post => {
				var postMeta = {};
				postMeta.slug = _post.slug;
				postMeta.title = _post.title;
				postMeta.summary = _post.summary;
				postMeta.date = _post.date;
				postMeta.thumbnail = _post.thumbnail;
				postMeta.categories = new Array();
				for(const [catSlug, category] of Object.entries(allCategories)) {
					for(const post in category.posts) {
						if(post.slug === _post.slug) {
							let shadCat = {};
							shadCat.slug = category.slug;
							shadCat.title = category.title;
							postMeta.categories.push(shadCat);
						}
					}
				}
				postList.push(postMeta);
			});
			return Promise.resolve(postList);
		}
		return loadProm.then(impl);
	}

	this.getAllCategories = function() {
		let impl = function() {
			let catlist = new Array();
			for(var catId in allCategories) {
				_L.info(catId);
				catlist.push(allCategories[catId]);
			}
			_L.info("category list: " + catlist);
			return Promise.resolve(catlist);
		}
		return loadProm.then(impl);
	}

	this.getCategory = function(catId) {
		let impl = function() {
			if(allCategories[catId]) {
				return Promise.resolve(allCategories[catId]);
			}
			return Promise.resolve(null);
		}
		return loadProm.then(impl);
	}

	//// HELPER METHODS ////////////////////////////////////////////////////////////////////////////

	function fetchRemoteData(path, format) {
		let prom = new Promise((resolve, reject) => {
			_L.debug("fetching definitions as " + format);
			$.ajax({
				cache: false, url: path, dataType: format,
				success: function(data) {
					_L.debug("definitions fetched from " + path);
					resolve(data);
				},
				error : function(xhdr, textStatus, error) {
					_L.error("error fetching definitions from " + path);
					_L.error("    status : " + textStatus);
					_L.error("    description : " + error);
					reject(path + " : " + textStatus + " - " + error);
				}
			});
		});
		return prom;
	}

	function parsePostsData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		let postsCol = XmlUtils.findCollection(rElm, "posts", "post");
		Array.from(postsCol).forEach(_post => {
			let post = {};
			post.slug = XmlUtils.toSimpleValue(_post, "slug");
			post.title = XmlUtils.toSimpleValue(_post, "title");
			post.summary = XmlUtils.toSimpleValue(_post, "summary");
			post.date = XmlUtils.toSimpleValue(_post, "date");
			post.thumbnail = XmlUtils.toSimpleValue(_post, "thumbnail");
			post.pinned = XmlUtils.hasValue(_post, "pinned");
			post.commentsEnabled = XmlUtils.hasValue(_post, "comments-enabled");
			allPosts.push(post);
		});
	}

	function parseCategoriesData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		let catsCol = XmlUtils.findRootCollection(rElm, "category");
		Array.from(catsCol).forEach(_cat => {
			let category = {};
			category.slug = XmlUtils.toSimpleValue(_cat, "slug");
			category.title = XmlUtils.toSimpleValue(_cat, "title");
			category.count = XmlUtils.toSimpleValue(_cat, "posts");
			allCategories[category.slug] = category;
		});
	}

	function loadEachCategory() {
		let promarr = new Array();
		for(const slug in allCategories) {
			let prom = fetchRemoteData("blog_categories/" + slug + "/index.xml", "xml").then(data => {
				parseEachCategory(slug, data);
			});
			promarr.push(prom);
		}
		return Promise.all(promarr);
	}

	function parseEachCategory(slug, xmlData) {
		_L.debug("parsing " + slug + " with " + xmlData);
		var rElm = XmlUtils.getRoot(xmlData);
		var category = allCategories[slug];
		if(typeof category === "undefined") {
			_L.error("that wasn't in the job description");
			return;
		}
		category.title = XmlUtils.toSimpleValue(rElm, "title");
		category.summary = XmlUtils.toSimpleValue(rElm, "summary");
		category.posts = new Array();
		let postsCol = XmlUtils.findCollection(rElm, "posts", "post");
		Array.from(postsCol).forEach(_post => {
			let post = {};
			post.slug = XmlUtils.toSimpleValue(_post, "slug");
			post.title = XmlUtils.toSimpleValue(_post, "title");
			post.summary = XmlUtils.toSimpleValue(_post, "summary");
			post.date = XmlUtils.toSimpleValue(_post, "date");
			post.thumbnail = XmlUtils.toSimpleValue(_post, "thumbnail");
			category.posts.push(post);
		});
	}
}

/** @global */
window.$blogData = blogData;

export default blogData;