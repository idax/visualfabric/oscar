import Logger from "/shared/logger/shared-logger.mod.js";
import { BookletPagePopulator } from "../../oscar/booklet/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("greenback.booklet.page");

var bookletPageAxns = new function() {

	var pop = null;

	this.loadLayout = function(page, params, query) {
		_L.info("booklet page layout loaded");
		pop = new BookletPagePopulator("pageTop");
		pop.setupLayout();
	}

	this.loadPage = function(page, params, query) {
		_L.info("booklet page loaded");
		if(pop != null) {
			pop.setup(page.slug);
		}
	}

	this.unloadPage = function() {
		_L.info("booklet page unloaded");
		if(pop != null) {
			pop.teardown();
		}
	}

	this.unloadLayout = function() {
		_L.info("booklet page layout unloaded");
		if(pop != null) {
			pop.teardownLayout();
		}
		pop = null;
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("bookletPageAxns", bookletPageAxns);
