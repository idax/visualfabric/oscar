import Logger from "/shared/logger/shared-logger.mod.js";
import { TocPopulator } from "../../oscar/booklet/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("booklet.toc");

var tocAxns = new function() {

	var tocPop = new TocPopulator("#bookletToc");

	this.loadPage = function() {
		tocPop.setup();
	}

	this.unloadPage = function() {
		tocPop.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("tocAxns", tocAxns);
