import Logger            from "/shared/logger/shared-logger.mod.js";
import ShortcutPopulator from "../../oscar/shortcut-populator.mod.js";
import latinum           from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.sitenav");

var siteNavAxns = new function() {

	var topcutPop = new ShortcutPopulator("#sitemenu-l1-top", "topnav");
	var botcutPop = new ShortcutPopulator("#sitemenu-l1-bot", "botnav");

	this.loadLayout = function(page, params, query) {
		_L.debug("layout loaded");
		topcutPop.setup();
	}

	this.loadPage = function(page, params, query) {
		_L.debug("page loaded");
		botcutPop.setup();
	}

	this.unloadLayout = function() {
		_L.debug("layout unloaded");
		topcutPop.teardown();
	}

	this.unloadPage = function() {
		_L.debug("page unloaded");
		botcutPop.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("siteNavAxns", siteNavAxns);
