import Logger  from "/shared/logger/shared-logger.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.statcounter");

var statCounterAxns = new function() {

	this.loadPage = function(page, params, query) {
		const stat_counter_url = "https://www.statcounter.com/counter/counter.js";

		_L.debug("page loaded");
		if(typeof(sc_project) != "undefined" && typeof(sc_security) != "undefined") {
			_L.debug("stat counter project: " + sc_project);
			_L.debug("stat counter security: " + sc_security);

			$("script").each(function() {
				var oldTag  = $(this);
				if(oldTag.attr("src") == stat_counter_url) {
					oldTag.remove();
				}
			});
			var newTag = $( "<script src='" + stat_counter_url + "' type='text/javascript' async></script>" );
			newTag.appendTo("head");
		}
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("statCounterAxns", statCounterAxns);
