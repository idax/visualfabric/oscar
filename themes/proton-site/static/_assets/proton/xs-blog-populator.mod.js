import Logger        from "/shared/logger/shared-logger.mod.js";
import { createApp } from "/shared/vue/vue.esm-browser.prod.js";
import XmlUtils      from "../oscar/xmlutils.mod.js";

var _L = Logger.get("proton.xsblog");

var XsBlogPopulator = function(nodeId) {

	if(nodeId.charAt(0) != '#') {
		nodeId = "#" + nodeId;
	}

	var dataLoc = "blog/index.xml";
	var count   = 0;
	var vapp    = null;
	var popData = null;

	this.setCount = function(val) {
		if(typeof val === "number") {
			count = val;
		}
	}

	this.setDataLocation = function(val) {
		if(typeof val === "string") {
			dataLoc = val;
		}
	}

/**
 * Method to be called whenever the corresponding latinum page gets loaded.
 */

	this.setup = function() {
		var vapp = createApp({
			data() {
				return {
					"posts" : new Array()
				};
			}
		});
		let vm = vapp.mount(nodeId);

		if(popData == null) {
			fetchRemoteData(dataLoc)
			.then(data => {
				parseData(data);
				updateTemplate(vm);
			}).catch(err => {
				_L.error(err);
			});
		}
		else {
			updateTemplate(vm);
		}
	}

/**
 * Method to be called whenever the corresponding latinum page gets unloaded.
 */

	this.teardown = function() {
		if(vapp != null) {
			vapp.unmount(nodeId);
			vapp = null;
		}
	}

	//// HELPER METHODS ////////////////////////////////////////////////////////////////////////////

	function fetchRemoteData(path, format) {
		let prom = new Promise((resolve, reject) => {
			_L.debug("fetching definitions as " + format);
			$.ajax({
				cache: false, url: path, dataType: format,
				success: function(data) {
					_L.debug("definitions fetched from " + path);
					resolve(data);
				},
				error : function(xhdr, textStatus, error) {
					_L.error("error fetching definitions from " + path);
					_L.error("    status : " + textStatus);
					_L.error("    description : " + error);
					reject(path + " : " + textStatus + " - " + error);
				}
			});
		});
		return prom;
	}

	function parseData(xmlData) {
		var rElm = XmlUtils.getRoot(xmlData);
		popData = {};
		popData.posts = new Array();
		let postsCol = XmlUtils.findCollection(rElm, "posts", "post");
		let limit = 0;
		Array.from(postsCol).forEach(_post => {
			if(count > 0 && limit >= count) {
				return;
			}
			let post = {};
			post.slug = XmlUtils.toSimpleValue(_post, "slug");
			post.title = XmlUtils.toSimpleValue(_post, "title");
			post.summary = XmlUtils.toSimpleValue(_post, "summary");
			post.date = XmlUtils.toSimpleValue(_post, "date");
			post.thumbnail = XmlUtils.toSimpleValue(_post, "thumbnail");
			popData.posts.push(post);
			limit++;
		});
	}

	function updateTemplate(vm) {
		popData.posts.forEach(post =>{
			vm.$data.posts.push(post);
		});
	}
}

export default XsBlogPopulator;