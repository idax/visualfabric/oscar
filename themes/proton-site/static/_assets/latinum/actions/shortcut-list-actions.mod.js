import Logger            from "/shared/logger/shared-logger.mod.js";
import ShortcutPopulator from "../../oscar/shortcut-populator.mod.js";
import latinum           from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.shortcut-list");

var shortcutListAxns = new function() {

	var theMap = {};

	this.loadLayout = function(page, params, query) {
		_L.info("page layout loaded");
	}

	this.unloadLayout = function() {
		_L.info("page layout unloaded");
	}

	this.loadPage = function(page, params, query) {
		if(!params["group"]) {
			return;
		}

		let group = params["group"];
		_L.info("page loaded for group : " + group);

		if(typeof theMap[group] !== "undefined") {
			theMap[group].setup();
		}
		else {
			theMap[group] = new ShortcutPopulator("#shortcut-list", group);
			theMap[group].setup();
		}
	}

	this.unloadPage = function(page, params, query) {
		_L.info("page unloaded");
		if(!params["group"]) {
			return;
		}

		let group = params["group"];
		_L.info(group);

		if(theMap[group]) {
			theMap[group].teardown();
		}
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("shortcutListAxns", shortcutListAxns);
