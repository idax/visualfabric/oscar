import Logger                from "/shared/logger/shared-logger.mod.js";
import { BookPagePopulator } from "../../oscar/book/populators.mod.js";
import latinum               from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.book.page");

var bookPageAxns = new function() {

	var pop = null;

	this.loadLayout = function(page, params, query) {
		_L.info("book page layout loaded");
		pop = new BookPagePopulator(latinum.siteId(), "pageTop", "pageBot");
		pop.setupLayout();
	}

	this.loadPage = function(page, params, query) {
		_L.info("book page loaded");
		if(pop != null) {
			pop.setup(page.slug);
		}
	}

	this.unloadPage = function() {
		_L.info("book page unloaded");
		if(pop != null) {
			pop.teardown();
		}
	}

	this.unloadLayout = function() {
		_L.info("book page layout unloaded");
		if(pop != null) {
			pop.teardownLayout();
		}
		pop = null;
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("bookPageAxns", bookPageAxns);
