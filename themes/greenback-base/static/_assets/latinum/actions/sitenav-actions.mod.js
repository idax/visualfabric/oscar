import Logger            from "/shared/logger/shared-logger.mod.js";
import ShortcutPopulator from "../../oscar/shortcut-populator.mod.js";
import latinum           from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("greenback.topnav");

var siteNavAxns = new function() {

	var topcutPop = new ShortcutPopulator("#topMenu", "topnav");
	var botcutPop = new ShortcutPopulator("#botMenu", "botnav");

	this.loadLayout = function(page, params, query) {
		_L.info("layout loaded");
		topcutPop.setup();
	}

	this.loadPage = function(page, params, query) {
		_L.info("page loaded");
		botcutPop.setup();
	}

	this.unloadLayout = function() {
		_L.info("layout unloaded");
		topcutPop.teardown();
	}

	this.unloadPage = function() {
		_L.info("page unloaded");
		botcutPop.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("siteNavAxns", siteNavAxns);
