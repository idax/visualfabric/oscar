import Logger from "/shared/logger/shared-logger.mod.js";
import { PinnedPostsPopulator, LatestPostsPopulator } from "../../oscar/blog/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.blog.welcome");

var welcomeAxns = new function() {

	var pinnedPop = new PinnedPostsPopulator("#pinnedPosts");
	pinnedPop.setCount(3);

	var latePop = new LatestPostsPopulator("#latestPosts");
	latePop.setCount(10);

	this.loadPage = function() {
		pinnedPop.setup();
		latePop.setup();
	}

	this.unloadPage = function() {
		pinnedPop.teardown();
		latePop.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context
latinum.registerAction("welcomeAxns", welcomeAxns);
