import Logger from "/shared/logger/shared-logger.mod.js";
import { AllCategoriesPopulator } from "../../oscar/blog/populators.mod.js";
import latinum from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.blog.categories");

var allCategoriesAxns = new function() {

	var allCat = new AllCategoriesPopulator("#allCategories");

	this.loadPage = function() {
		allCat.setup();
	}

	this.unloadPage = function() {
		allCat.teardown();
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("allCategoriesAxns", allCategoriesAxns);
