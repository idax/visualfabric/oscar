import Logger                      from "/shared/logger/shared-logger.mod.js";
import { SingleCategoryPopulator } from "../../oscar/blog/populators.mod.js";
import latinum                     from "/shared/latinum/latinum.mod.js";

var _L = Logger.get("proton.blog.onecat");

var singleCategoryAxns = new function() {

	var pop = null;

	this.loadPage = function(page, params, query) {
		_L.info("page loaded");

		if(!params["catslug"]) {
			return;
		}
		let catslug = params["catslug"];
		_L.info("list posts for category : " + catslug);

		pop = new SingleCategoryPopulator("#postsByCategory", catslug);
		pop.setItemsPerPage(10);
		pop.setup();
	}

	this.unloadPage = function() {
		if(pop != null) {
			pop.teardown();
		}
		pop = null;
	}
}

// Now register the above object with the action methods into latinum's action context

latinum.registerAction("singleCategoryAxns", singleCategoryAxns);
